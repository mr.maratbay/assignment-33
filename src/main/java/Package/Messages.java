package Package;

public class Messages {
    private int id;
    private static int id_gen = 0;

    private int from;
    private int ab;
    private String message;

    public Messages(int from, int to, String message){
        generateId();
        setFrom(from);
        setAb(ab);
        setMessage(message);
    }

    private void generateId(){
        id = id_gen++;
    }

    public int getFrom(){
        return from;
    }

    public void setFrom(int from){
        this.from = from;
    }

    public int getAB(){
        return ab;
    }

    public void setAb(int ab){
        this.ab = ab;
    }

    public String getMessage(){
        return message;
    }

    public void setMessage(String message){
        this.message = message;
    }

}
